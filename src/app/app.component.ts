import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { MessageService } from 'primeng/api'
import { BehaviorSubject } from 'rxjs';
import { TodoService } from './services/todo.service';
import { selectorGetDone, selectorGetProgress, selectorGetReview, selectorGetTodo } from './store/task/task.reducer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Todo App';

  getSelectedTask: any;
  getStatus:any;

  todo: any
  progress: any
  review: any
  done: any

  constructor(
    private taskService: TodoService,
    private store: Store
  ) {
    this.todo = store.select(selectorGetTodo)
    this.progress = store.select(selectorGetProgress)
    this.review = store.select(selectorGetReview)
    this.done = store.select(selectorGetDone)
  }

  ngOnInit(): void {
    this.taskService.getTasks()
  }

  updateTask(payload: any) {
    this.taskService.updateTask(payload?.status, payload.id)
  }

  getTask(task:any) {
    this.updateTask({
      id: task?._id, 
      status: this.getStatus
    })
  }

  onDrop(status: any) {
    this.getStatus = status
  }
}
