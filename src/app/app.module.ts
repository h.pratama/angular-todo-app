import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng/button'
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api'
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { CardModule } from 'primeng/card'
import {DividerModule} from 'primeng/divider';
import {DragDropModule} from 'primeng/dragdrop';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { taskReducer } from './store/task/task.reducer';
import { CardComponent } from './components/card/card.component';
import { TodoService } from './services/todo.service';
import { HomeComponent } from './home/home.component';
import { AppStoreModule } from './store';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    AppStoreModule,
    ButtonModule,
    MessageModule,
    MessagesModule,
    ToastModule,
    CardModule,
    DividerModule,
    DragDropModule,
  ],
  providers: [
    HttpClient,
    MessageService,
    TodoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
