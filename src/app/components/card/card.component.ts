import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Task } from 'src/app/store/task/task.reducer';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() task:any;
  @Input() header:string = '';

  @Output() updateTask = new EventEmitter()
  @Output() getSelectedTask = new EventEmitter()

  selectedTask: any = null;

  constructor(
    private store: Store
  ) {
  }

  ngOnInit(): void {
    this.selectedTask = null;
  }

  update(status:string, id:string) {
    let getStatus = ''
    if (status === 'todo' ) getStatus = 'progress' 
    if (status === 'progress' ) getStatus = 'review' 
    if (status === 'review' ) getStatus = 'done' 

    if (getStatus.length > 0) this.updateTask.emit({status: getStatus, id: id})
  }

  onDragStart(payload:Task) {
    this.selectedTask = payload
  }

  dragEnd(){
   if (this.selectedTask) {
    this.getSelectedTask.emit(this.selectedTask)
    this.selectedTask = null
   } 
  }
}
