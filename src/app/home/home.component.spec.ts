import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sould be have content "home works!" ', () => {
    const compiled = fixture.nativeElement as HTMLElement
    expect(compiled.querySelector('p')?.textContent).toContain('home works!')
  })

  it('button Changes event click should be changes isOn property', () => {
    const compiled = fixture.nativeElement as HTMLElement
    const getTextMessage = compiled.getElementsByClassName('message')[0].textContent

    expect(component.message).toContain('The light is mati')
    expect(component.isOn)
      .withContext('nilai awal false')
      .toBe('mati')
    component.clicked()
        
    expect(component.message).toContain('The light is hidup')
    expect(component.isOn)
      .withContext('valuenya bakal berubah jadi true')
      .toBe('hidup')

    
    component.clicked()
    expect(component.message).toContain('The light is mati')
    expect(component.isOn)
      .withContext('kembali ke nilai awal')
      .toBe('mati')
  })

});
