import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isOn:string = 'mati'

  constructor() { }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    console.log('destroy');
    
  }

  clicked() {
    this.isOn = this.isOn == 'mati' ? 'hidup' : 'mati'
  }

  get message() { return `The light is ${this.isOn}`; }
}
