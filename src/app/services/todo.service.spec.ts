import { HttpClient, HttpClientModule } from "@angular/common/http"
import { TestBed } from "@angular/core/testing"
import { ActionsSubject, ReducerManager, ReducerManagerDispatcher, Store, StoreModule } from "@ngrx/store";
import { MessageService } from "primeng/api";
import { taskReducer } from "../store/task/task.reducer";
import { TodoService } from './todo.service';

describe('TodoService', () => {

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                HttpClientModule,
                StoreModule.forRoot({
                    task: taskReducer
                })
            ],
            providers: [
                TodoService,
                Store,
                MessageService,
                ActionsSubject,
                ReducerManager,
                ReducerManagerDispatcher
            ]
        })
    })

    it('should be created', () => {
        const service: TodoService = TestBed.get(TodoService);
        expect(service).toBeTruthy();
    });

    it('req to api', (done: DoneFn) => {
        const service: TodoService = TestBed.get(TodoService);

        expect(service.getTasks())
        .toBe(null)

        service.reqTask().subscribe((resp:any) => {
            
            expect(resp?.status)
            .withContext('expected status response')
            .toBe(200)

            expect(resp?.data?.length)
            .withContext('expected get data')
            .toBeGreaterThan(0)

            done()
        })
    });
    
    it('update task', (done: DoneFn) => {
        const service: TodoService = TestBed.get(TodoService);
        
        expect(
            service.updateTask('done', '634ff16954153beab6eb5de0')
        ).toBe(null)

        service.reqUpdateTask('done', '634ff16954153beab6eb5de0')
        .subscribe((resp:any) => {

            expect(resp?.status)
            .withContext('expect status response')
            .toBe(200)

            expect(resp?.ok)
            .withContext('expect ok response')
            .toBeTruthy()

            expect(resp?.message)
            .withContext('expect message response')
            .toEqual('Success update task')

            done()
        })
    })

})

