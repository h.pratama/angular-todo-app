import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http'
import { Store } from '@ngrx/store';
import { setDone, setProgress, setReview, setTodo } from '../store/task/task.actions';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(
    private http: HttpClient,
    private store: Store,
    private messages: MessageService
  ) { }

  urlApi = 'https://doku-todo-app.herokuapp.com/task';

  getTasks():null {
    this.http.get(this.urlApi).subscribe((data:any) => {
      if (data?.status === 200) {
        let getTodoTask:any = []
        let getProgressTask:any = []
        let getReviewTask:any = []
        let getDoneTask:any = []

        data?.data?.map((task:any) => {
          if (task?.status == 'todo') getTodoTask.push(task)
          if (task?.status == 'progress') getProgressTask.push(task)
          if (task?.status == 'review') getReviewTask.push(task)
          if (task?.status == 'done') getDoneTask.push(task)
        })

        this.store.dispatch(setTodo({data: getTodoTask}))
        this.store.dispatch(setProgress({data: getProgressTask}))
        this.store.dispatch(setReview({data: getReviewTask}))
        this.store.dispatch(setDone({data: getDoneTask}))
      }
    })

    return null
  }

  updateTask(status:string, id: string):null {
    this.http.put(
      `${this.urlApi}/${id}`, 
      {status},
    ).subscribe((resp:any) => {
      if (resp?.status === 200) {
        this.messages.add({
          severity:'success', 
          summary:`Success update status task to ${status}`,
        })
        
        this.getTasks()

      }
    })

    return null
  }

  reqTask() {
    return this.http.get(this.urlApi)
  }

  reqUpdateTask(status:string, id: string) {
    return this.http.put(`${this.urlApi}/${id}`, {status})
  }

}
