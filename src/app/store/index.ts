import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { taskReducer } from "./task/task.reducer";

const reducers = {
    task: taskReducer,
    // user: userReducer
    // news: newsReducer
}

@NgModule({
    imports: [
        StoreModule.forRoot(reducers)
    ],
    exports: [StoreModule]
})

export class AppStoreModule {}
