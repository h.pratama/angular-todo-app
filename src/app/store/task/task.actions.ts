import { createAction, props } from '@ngrx/store';

export const setTodo = createAction(
    '[Task State] set todo',
    props<{data: Task[]}>()
);
export const setProgress = createAction(
    '[Task State] set progress',
    props<{data: Task[]}>()
);
export const setReview = createAction(
    '[Task State] set review',
    props<{data: Task[]}>()
);
export const setDone = createAction(
    '[Task State] set done',
    props<{data: Task[]}>()
);
