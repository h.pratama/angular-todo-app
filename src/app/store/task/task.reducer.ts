import { createFeatureSelector, createSelector, createReducer, on } from '@ngrx/store';
import { setDone, setProgress, setReview, setTodo } from './task.actions';

export interface Task {
    _id: string,
    title: string,
    desc: string,
    status: string,
    __v: number;
}

export interface TodoState {
    todo: Task[];
    progress: Task[];
    review: Task[];
    done: Task[];
}

const initialState: TodoState = {
    todo: [],
    progress: [],
    review: [],
    done: [],
}

export const taskReducer = createReducer(
    initialState,
    on(setTodo, (state: TodoState, payload:any) => ({...state, todo: payload.data })),
    on(setProgress, (state: TodoState, payload:any) => ({...state, progress: payload.data })),
    on(setReview, (state: TodoState, payload:any) => ({...state, review: payload.data })),
    on(setDone, (state: TodoState, payload:any) => ({...state, done: payload.data })),
  );


export const taskSelector = createFeatureSelector<TodoState>('task')

export const selectorGetTodo = createSelector(
    taskSelector,
    (state:TodoState) => state.todo
)
export const selectorGetProgress = createSelector(
    taskSelector,
    (state:TodoState) => state.progress
)
export const selectorGetReview = createSelector(
    taskSelector,
    (state:TodoState) => state.review
)
export const selectorGetDone = createSelector(
    taskSelector,
    (state:TodoState) => state.done
)
